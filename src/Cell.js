import {
  state,
  canvas,
  ctx
} from "./state";

export class Cell {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.walls = [true, true, true, true];
    this.visited = false;
    this.exit = false;

    this.state = state();
  }

  index(x, y) {
    if (x < 0 || y < 0 || x > this.state.cols - 1 || y > this.state.rows - 1) {
      return -1;
    }
    return x + y * this.state.cols;
  }

  checkNeighbours(grid) {
    let neighbors = [];

    let top = grid[this.index(this.x, this.y - 1)];
    let right = grid[this.index(this.x + 1, this.y)];
    let bottom = grid[this.index(this.x, this.y + 1)];
    let left = grid[this.index(this.x - 1, this.y)];

    if (top && !top.visited) {
      neighbors.push(top);
    }
    if (right && !right.visited) {
      neighbors.push(right);
    }
    if (bottom && !bottom.visited) {
      neighbors.push(bottom);
    }
    if (left && !left.visited) {
      neighbors.push(left);
    }

    if (neighbors.length > 0) {
      let rand = Math.floor(Math.random() * neighbors.length);
      return neighbors[rand];
    } else {
      return undefined;
    }
  }

  highlightCell(exit) {
    const wh = this.state.wh;

    let x = this.x * wh;
    let y = this.y * wh;

    if (exit) {
      ctx.fillStyle = "#ff0000";
      ctx.fillRect(x, y, wh, wh);
    } else {
      ctx.fillStyle = "#ffffff";
      ctx.fillRect(x, y, wh, wh);
    }
  }

  makeWalls() {
    const wh = this.state.wh;

    let x = this.x * wh;
    let y = this.y * wh;

    if (this.walls[0]) {
      ctx.beginPath();
      ctx.lineWidth = 1;
      ctx.moveTo(x, y);
      ctx.lineTo(x + wh, y);
      ctx.stroke();
    }

    if (this.walls[1]) {
      ctx.beginPath();
      ctx.lineWidth = 1;
      ctx.moveTo(x + wh, y);
      ctx.lineTo(x + wh, y + wh);
      ctx.stroke();
    }

    if (this.walls[2]) {
      ctx.beginPath();
      ctx.lineWidth = 1;
      ctx.moveTo(x + wh, y + wh);
      ctx.lineTo(x, y + wh);
      ctx.stroke();
    }

    if (this.walls[3]) {
      ctx.beginPath();
      ctx.lineWidth = 1;
      ctx.moveTo(x, y + wh);
      ctx.lineTo(x, y);
      ctx.stroke();
    }

    if (this.visited) {
      ctx.fillStyle = "#ff0000";
      ctx.fillRect(x, y, wh, wh);
    }
  }
}