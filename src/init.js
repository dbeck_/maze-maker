import {
  canvas,
  state
} from "./state";
import {
  Game
} from "./Game";
import {
  Cell
} from "./Cell";

class Setup {
  constructor() {
    this.grid = [];
    this.state = state();
  }

  setup() {
    canvas.style.backgroundColor = '#000000';
  }

  makeGrid() {
    let grid = [];
    for (let r = 0; r < this.state.rows; r++) {
      for (let c = 0; c < this.state.cols; c++) {
        let cell = new Cell(c, r);
        grid.push(cell);
      }
    }
    this.grid = grid;
    this.current = this.grid[0];
  }

  init() {
    this.setup();
    this.makeGrid();

    new Game(this.current, this.grid)
      .animate();

  }
}

new Setup()
  .init();