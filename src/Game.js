export class Game {
  constructor(current, grid) {
    this.grid = grid;
    this.current = current;
    this.stack = [];
    this.fps = 60;
    this.runs = true;
  }

  animate() {
    if (this.runs) {
      setTimeout(() => {
        requestAnimationFrame(() => {
          this.draw();
        });
      }, 1000 / this.fps);
    }
  }

  removeWalls(x, y) {
    let dx = x.x - y.x;

    if (dx === 1) {
      x.walls[3] = false;
      y.walls[1] = false;
    } else if (dx === -1) {
      x.walls[1] = false;
      y.walls[3] = false;
    }
    let dy = x.y - y.y;

    if (dy === 1) {
      x.walls[0] = false;
      y.walls[2] = false;
    } else if (dy === -1) {
      x.walls[2] = false;
      y.walls[0] = false;
    }
  }

  printConfig(data) {
    const fetchResults = document.getElementById("print-results");
    const dataField = document.getElementById("data");
    // show button when done
    fetchResults.disabled = false;
    fetchResults.style.display = "block";
    fetchResults.addEventListener("click", e => {
      dataField.innerHTML = data;
    });
  }

  draw() {
    for (let i = 0; i < this.grid.length; i++) {
      this.grid[i].makeWalls();
      this.grid[this.grid.length - 1].exit = true;
    }

    this.current.visited = true;

    let next = this.current.checkNeighbours(this.grid);

    if (next) {
      next.visited = true;
      this.stack.push(this.current);
      this.removeWalls(this.current, next);

      this.current = next;
    } else if (this.stack.length > 0) {
      // when stuck with no neighbours, track back
      this.current = this.stack.pop();
    } else if ((this.stack[0] = this.grid[0])) {
      // when done
      this.runs = false;
      this.current.visited = true;
      this.current.highlightCell(true);
      this.printConfig(JSON.stringify(this.grid));
    }
    this.animate();
  }
}
