const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");

function state() {
  let cols, rows, wh = 20;
  cols = Math.floor(ctx.canvas.width / wh);
  rows = Math.floor(ctx.canvas.height / wh);
  return {
    cols,
    rows,
    wh
  }
}

export {
  state,
  canvas,
  ctx
}